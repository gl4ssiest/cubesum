<?
public function post_confirm(){

	$id = Input::get( 'service_id' );
	$servicio = Service::find( $id );
	if(!is_null( $servicio )){   

		if($servicio -> status_id == '6'){ 
			return Response::json(array( 'error' => '2' )); 
		}elseif(!is_null( $servicio -> driver_id ) && $servicio -> status_id == '1'){
			// metodo update() puede ser invocado desde el mismo objeto.
			if( $servicio -> update ( array ( 'driver_id' => Input::get( 'driver_id' ),'status_id' => '2' ))){
				if(empty( $servicio -> user -> uuid )){
					return Response::json(array('error' => '0'));
				}
				$push = Push::make();
				$pushMessage = 'Tu servicio ha sido confirmado';
				if($servicio -> user -> type == '1') { //iphone
					$result = $push -> ios( $servicio -> user -> uuid, $pushMessage, 1, 'honk.wav', 'Open', array('serviceId' => $servicio -> id));
				} else {
					$result = $push -> android2( $servicio -> user -> uuid, $pushMessage, 1, 'default', 'Open', array('serviceId' => $servicio -> id));
				}
				return Response::json( array( 'error' => '0' ) );
			}else
				return Response::json( array( 'error' => '99' ) ); //Error correspondiente a fallo interno en clase Service
		}else
			return Response::json( array( 'error' => '1' ) );
	}else
		return Response::json( array( 'error' => '3' ) );
	
}

/**
 *
 *
 *
 * 
 * Esta funcion va en clase Service, y separa responsabilidades. Esta Clase actualiza los atributos 
 * del objeto Servicio, dependiendo de los parametros que recibe
 * @param  array  $params arreglo de atributos que se van a actualizar.
 * @return bool   Se ejecuto o no con exito la funcion.
 */
public static function update( $params = array()){
	if(!empty( $params )){
		if(isset( $params['driver_id'] )){
			/**
			 * probablemente Driver sera una clase manejadora de BD, y aqui esta cambiando el estado del registro en "avaliable" a 0 - Conductor No Disponible
			 */
			$driverTmp = Driver::find( $params ['driver_id']  );
			$driverTmp->update( $this->id, array(
				'avaliable' => '0',
			));
			$this -> car_id = $driverTmp -> car_id;
		}
		if(isset( $params['status_id'] )){
			$this -> status_id = $params['status_id'];
		}	
		return true;
	}else
		return false;
}

?>