<?php

return array(
	'name'=>'Backend Rappi Test',
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),
	'defaultController'=>'test',
	'components'=>array(
		'urlManager'=>array(
			'urlFormat'=>'path',
			/*'rules'=>array(
				'test/result/<g:\w>'=>'test/result',
			),*/
		),
	),
);