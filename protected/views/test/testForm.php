<div class="col-md-6">
	<p><strong>Input Format: </strong> <br>
	The first line contains an integer <i>T</i>, the number of test-cases. <i>T</i> testcases follow. 
	For each test case, the first line will contain two integers <i>N</i> and <i>M</i> separated by a single space. 
	<br>
	<i>N</i> defines the <i>N x N x N</i> matrix.
	<br> 
	<i>M</i> defines the number of operations.
	<br> 
	The next <i>M</i> lines will contain either:</p>

	<pre>
	<code>1. UPDATE x y z W
	2. QUERY  x1 y1 z1 x2 y2 z2 
	</code>
	</pre>

	<p><strong>Sample Input: </strong></p>
	<pre>
	<samp>2
	4 5
	UPDATE 2 2 2 4
	QUERY 1 1 1 3 3 3
	UPDATE 1 1 1 23
	QUERY 2 2 2 4 4 4
	QUERY 1 1 1 3 3 3
	2 4
	UPDATE 2 2 2 1
	QUERY 1 1 1 1 1 1
	QUERY 1 1 1 2 2 2
	QUERY 2 2 2 2 2 2
	</samp>
	</pre>
</div>


<div class="col-md-6">
	<div class="form">

	<?php $form=$this->beginWidget('CActiveForm'); ?>
		
		
		<div class="form-group">
			<p><strong>Input: </strong> <br></p>
			<?php echo $form->textArea($formModel,'textInput',array('rows'=>10, 'cols'=>75)); ?>
		</div>

		<?php echo $form->errorSummary($formModel); ?>

		<div class="form-group submit">
			<?php echo CHtml::submitButton('Submit', array("class"=>"btn btn-default")); ?>
		</div>

	<?php $this->endWidget(); ?>

	</div><!-- form -->

	<?php if(!empty($formModel->result) && $formModel->printResult){


		foreach ($formModel->result as $result){

			echo($result.'<br>');

		}

    }
	 ?>
</div>
