<?php

/**
 * CaptureForm class.
 */
class CaptureForm extends CFormModel
{
	public $textInput;
	public $result;
	public $printResult = false;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('textInput', 'required'),
			array('textInput', 'inputValidator'),
		);
	}
	
	/**
	 * Declares attribute labels.
	 */	
	public function attributeLabels()
	{
		return array(
			'textInput'=>'Input',
		);
	}

	public function inputValidator($attribute,$params)
	{
		$textAreaLineArray = $this->textToLines($this->textInput ." "); //bugfix
		$firstLine = substr($textAreaLineArray[0], 0, -1);

		if( !is_numeric($firstLine) || (int)$firstLine < 1 || (int)$firstLine > 50 ){
			$this->addError('textInput','Primera linea no es un numero entre 1 y 50.'.$firstLine);
		}else{
			$testCasesToComplete = $firstLine;
			$testCasesCompleted = 0;
			array_splice($textAreaLineArray,0,1);
		}

		do {

            $testCaseHeader = preg_split('/ +/', $textAreaLineArray[0]);
			if(count($testCaseHeader) != 2){
				$this->addError('textInput','La cabecera del TestCase esta en formato incorrecto. Longitud de Cabecera.'. count($testCaseHeader) );
				return false;
			}

			if(is_numeric($testCaseHeader[0]) && $testCaseHeader[0] >= 1 && $testCaseHeader[0] <= 100)
				$matrixSize = $testCaseHeader[0];
			else{
				$this->addError('textInput','La cabecera del TestCase esta en formato incorrecto. Tamaño de Matriz.');
				return false;				
			}

			$testCaseHeader[1] = substr($testCaseHeader[1], 0, -1);
			if(is_numeric($testCaseHeader[1]) && $testCaseHeader[1] >= 1 && $testCaseHeader[1] <= 1000)
				$numberOfOperations = $testCaseHeader[1];
			else{
				$this->addError('textInput','La cabecera del TestCase esta en formato incorrecto. Numero de Operaciones: '. $testCaseHeader[1] );
				return false;				
			}

			array_splice($textAreaLineArray,0,1); //no need of header anymore

			try{
				$testCaseOperations = array_splice($textAreaLineArray,0,$numberOfOperations);
			}catch(Exception $e){
				$this->addError('textInput','Error en numero de Operaciones del TestCase.');
				return false;
			}

			if($this->runTestCase($matrixSize,$numberOfOperations,$testCaseOperations))
				$testCasesCompleted++;
			else
				return false;	



		} while (count($textAreaLineArray) > 0);

		if($testCasesCompleted != $testCasesToComplete){
			$this->addError('textInput','Introdujo diferente numero de TestCases a lo especificado en la primera Linea.');
			return false;
		}

		return true;
	}

	/**
	 * Textarea convertido en Arreglo de "lineas"
	 * @param  string $str Textarea String
	 * @return array      arreglo de lineas
	 */
	protected function textToLines($str = ''){

		$textAr = explode("\n", $str);
		$textAr = array_filter($textAr, 'trim');
		return $textAr;	

	}

	/**
	 * Se ejecuta el TestCase
	 * @param  integer $matrixSize         Tamaño de la Matriz
	 * @param  integer $numberOfOperations Numero de Operaciones
	 * @param  integer $testCaseOperations Arreglo de Operaciones
	 * @return boolean                     Si corrio o no.
	 */
	protected function runTestCase($matrixSize=0,$numberOfOperations=0,$testCaseOperations=array()){

		$matrix = new Matrix($matrixSize);

		foreach($testCaseOperations as $operation){

			$operation = substr($operation, 0, -1);
			$operationParts = preg_split('/ +/', $operation);

			 for($i = 1 ; $i<= count($operationParts) -1; $i++){

			 	if(!is_numeric($operationParts[$i])){
			 		$this->addError('textInput','Valores no numericos en  '. $operation );
			 		return false;
			 	}
			 }

			 if($operationParts[0] == "UPDATE" && count($operationParts) == 5){

			 	$matrix->update($operationParts[1], $operationParts[2], $operationParts[3], $operationParts[4]);
			 	$this->result [] = $operation .' : OK';

			 } else if ($operationParts[0] == "QUERY" && count($operationParts) == 7){

			 	$query = $matrix->query($operationParts[1], $operationParts[2], $operationParts[3], $operationParts[4], $operationParts[5], $operationParts[6]);
			 	$this->result [] = $operation .' : '.$query;

			 } else {

			 	$this->result [] = $operation .' : FAILED';
			 	$this->addError('textInput','Error en Operacion: '. $operation );
			 	return false;

			 }

		}

		$this->result [] = '-------';
		return true;

	}


}
