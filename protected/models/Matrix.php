<?php

class Matrix
{
	/**
	 * @var integer Tamaño de la matriz
	 */
	public $size;
	/**
	 * @var array Arreglo de valores 
	 */
	protected $values = array();

	public function __construct($N) {
       $this->setInitMatrix($N);
       $this->size = $N;
    }

    protected function setInitMatrix($N){

    	for ($m = 1; $m <= $N; $m++){

    		$slice = array();

	    	for ($i = 1; $i <= $N; $i++){

	    		$row = array();

		   		for ($j = 1; $j <= $N; $j++){
		   			$row [$j] = 0;
	    		}

	    		$slice [$i] = $row;

	    	}

	    	$this->values [$i] = $slice;

        }

    }

    /**
     * Funcion para actualizar el valor de una coordenada dentro de la matriz
     * @param  integer $x [description]
     * @param  integer $y [description]
     * @param  integer $z [description]
     * @param  integer $W [description]
     * @return [type]     [description]
     */
    public function update($x = 1,$y = 1,$z = 1,$W = 1){

    	if($W < 100000000 && $W > -100000000)
	    	$this->values[$x][$y][$z] = $W;
	    else
	    	return false;

    }


    /**
     * Esta funcion usa el algoritmo del camino mas corto por busqueda, asumiendo que el 
     * camino mas corto entre una coordenada y otra tiene como longitud el valor mas grande 
     * de las diferencias entre coordenadas + 1 (por la condicion inclusive) 
     * 
     * ej. entre 1,1,1 y 3,2,1, el camino mas corto tendria como longitud el mayor numero entre 
     *
     * 3-1=2 +1 = 3, 2-1=1 +1 =2 , 1-1=1  +1=2   ===> max(3,2,2) = 3
     *
     * Este seria el numero de pasos maximo en la matriz que el loop va a avanzar verificando que la 
     * coordenada 1 sea igual a coord. 2 
     * @param  integer $x1 [description]
     * @param  integer $y1 [description]
     * @param  integer $z1 [description]
     * @param  integer $x2 [description]
     * @param  integer $y2 [description]
     * @param  integer $z2 [description]
     * @return integer $result  La sumatoria de los valores de las coordenadas recorridas
     */
    public function query($x1 = 1,$y1 = 1,$z1 = 1,$x2 = 1,$y2 = 1,$z2 = 1){

    	$difx = $x2 - $x1; 
    	$dify = $y2 - $y1;
    	$difz = $z2 - $z1;

    	$maxNumberOfSteps = max($difx, $dify, $difz) + 1; //inculsive requirement
    	$result = ''; //init result

    	for ($i = 0; $i <= $maxNumberOfSteps; $i++){

    		$result = $result + $this->values[$x1][$y1][$z1];

    		if($x1 != $x2)
    			$x1 = $x1 + 1;

    		if($y1 != $y2)
    			$y1 = $y1 + 1;

    		if($z1 != $z2)
    			$z1 = $z1 + 1;

    		if($x1 == $x2 && $y1 == $y2 && $z1 == $z2)
    			break;

    	}

    	return $result;

    }

}